# README #

## Catch of the Day Application ##
This is a real time application for a trendy seafood market where *price* and *quantity available* are variable and can change at moment's notice. This concept is inspired from https://reactforbeginners.com/ tutorial.  

**Requirements:**

* A menu system to show items and add items if available.
* An order form system to show and remove ordered items
* An inventory management system to show, add and update items
* Use AngularJs / HTML / CSS (SASS) as the frontend

**Demo:**
http://ng-order.slickcodes.com

## Commercial Use Cases ##
* Take away orders
* Fresh seafood market