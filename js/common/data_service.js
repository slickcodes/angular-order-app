// angular service to share data across different controllers
(function(){

	var app = angular.module('orderApp');

	app.service('DataService', function(){

		// this service
		var _this = this;

		// lists of items
		_this.lists = [];

		// ordered items
		_this.orders = [];

	});

})();