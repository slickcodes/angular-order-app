(function(){
	// main app
	var app = angular.module('orderApp', []);

	// this controller is used for displaying and adding items
	app.controller('menuController', function($scope, DataService){

		// this controller
		var _this = this;

		// reference to the shared item list
		$scope.lists = DataService.lists;
		$scope.orders = DataService.orders;

		// add to order form
		$scope.addOrder = function(_index){
			DataService.orders.push(DataService.lists[_index]);
			console.log(DataService.orders);
		}
	});

	// this controller is used for displaying and removing items
	app.controller('orderController', function($scope, DataService){

		// this controller
		var _this = this;

		// reference to the shared item list
		$scope.orders = DataService.orders;

		// this function will remove the item from the ordered list
		$scope.removeOrder = function(_index){
			DataService.orders.splice(_index,1);
		}

		// this function calculates the total ordered price
		$scope.totalOrder = function(){
			var total = 0;
			var price;

			for(x = 0; x < DataService.orders.length; x++){
				price = DataService.orders[x].price;
				
				// check if price is empty
				if(price === null || price === '' || DataService.orders[x].available === 'false'){
					price = 0;
				}

				// add all total price
				total += parseFloat(price);
			}

			return total;
		}
	});


	// this controller is used for adding, updating, and removing items on the menu list
	app.controller('inventoryController', function($scope, DataService){

		// this controller
		var _this = this;

		// reference to the shared item list
		$scope.lists = DataService.lists;

		// this function will add an item into a list
		$scope.addItem = function(){
			// create an item object
			var _item = {
				name : $scope.name,
				price : $scope.price,
				available : $scope.availability,
				description : $scope.description,
				photo : $scope.photo
			};

			// add to the DataService item list
			DataService.lists.push(_item);

			// clear the form
			$scope.name = null;
			$scope.price = null;
			$scope.description = null;
			$scope.photo = null;
			$scope.availability = null;
		}

		// this function will remove an item from a list
		$scope.removeItem = function(_index){
			// remove index from a list
			var _confirm = confirm("Are you sure you want to remove this fish!?");
			if(_confirm){
				$scope.lists.splice(_index,1);
			}
		}

		// this function will load samples into the list
		$scope.loadSamples = function(){
			
			// this will hold the sample items
			_this.sample = [
				{
					name : 'Pacific Halibut',
					price : 17.24,
					available : 'true',
					description : 'Everyones favorite white fish. We will cut it to the size you need and ship it.',
					photo : 'http://i.istockimg.com/file_thumbview_approve/36248396/5/stock-photo-36248396-blackened-cajun-sea-bass.jpg'
				},{
					name : 'Lobster',
					price : 32.00,
					available : 'true',
					description : 'These tender, mouth-watering beauties are a fantastic hit at any dinner party.',
					photo : 'http://i.istockimg.com/file_thumbview_approve/32135274/5/stock-photo-32135274-cooked-lobster.jpg'
				},{
					name : 'Sea Scallops',
					price : 16.84,
					available : 'true',
					description : 'Big, sweet and tender. True dry-pack scallops from the icey waters of Alaska. About 8-10 per pound',
					photo : 'http://i.istockimg.com/file_thumbview_approve/58624176/5/stock-photo-58624176-scallops-on-black-stone-plate.jpg'
				}
			];

			// push to the DataService item list
			for(x = 0; x < _this.sample.length; x++){
				DataService.lists.push(_this.sample[x]);
			}
		}

		// prepopulate items
		$scope.loadSamples();

	});

})();
